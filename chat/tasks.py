from django.urls import reverse
from django.core.mail import send_mail
from messenger.celery import app
 
from .models import ChatSessionMessage
 
@app.task
def send_notification_email(message_id):
    message = ChatSessionMessage.objects.get(pk=message_id)

    send_mail(
        "Message from {0}".format(message.user.username),
        "Message: {0}".format(message.message),
        'noreply@messenger.dev',
        [message.to_user.email],
        fail_silently=False,
    )
