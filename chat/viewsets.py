import json

from django.db.models import Q
from django.contrib.auth import get_user_model
from .tasks import send_notification_email
User = get_user_model()

#from anymail.message import AnymailMessage
from rest_framework import viewsets, serializers, response, status, permissions, filters
from rest_framework.decorators import detail_route, list_route

from .models import ChatSession, ChatSessionMessage
from .serializers import ChatSessionSerializer, ChatSessionMessageSerializer, UserSerializer

class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.none()
    serializer_class = UserSerializer
    permission_class = (permissions.IsAuthenticated(), )
    filter_backends = (filters.SearchFilter,)
    search_fields = ('username', 'email', )

    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return User.objects.none()
        return User.objects.exclude(username=self.request.user.username)

class ChatSessionViewSet(viewsets.ModelViewSet):
    queryset = ChatSession.objects.none()
    serializer_class = ChatSessionSerializer
    permission_class = (permissions.IsAuthenticated(), )
    filter_backends = (filters.SearchFilter,)
    search_fields = ('messages__message',)

    def get_queryset(self):
        if not self.request.user.is_authenticated():
            return ChatSession.objects.none()
        return ChatSession.objects.filter(Q(created_by=self.request.user) | Q(received_by=self.request.user))
        
    
    def create(self, request):
        data = json.loads(request.body)
        created_by = request.user
        received_by = User.objects.get(username=data.get('received_by'))
        session = ChatSession.objects.create(created_by=created_by, received_by=received_by)
        
        if data.get('message'): 
            message = ChatSessionMessage.objects.create(user=request.user, message=data.get('message'), chat_session=session)
            send_notification_email.delay(message.id)
        
        serializer = ChatSessionSerializer(session, context={'request': request})
        return response.Response(serializer.data, status=status.HTTP_201_CREATED)
    
    @detail_route(methods=['post'])
    def message(self, request, pk=None):
        session = self.get_object()
        data = json.loads(request.body)

        message = ChatSessionMessage.objects.create(user=request.user, message=data.get('message'), chat_session=session)
        send_notification_email.delay(message.id)
        serializer = ChatSessionSerializer(session, context={'request': request})

        return response.Response(serializer.data, status=status.HTTP_201_CREATED)
    
    # This is for marking session as read
    @detail_route(methods=['put'])
    def messages(self, request, pk=None):
        session = self.get_object()

        for message in session.messages.filter(read=False).exclude(user=self.request.user):
            message.read = True
            message.save()

        serializer = ChatSessionSerializer(session, context={'request': request})
        
        return response.Response(serializer.data)

class ChatSessionMessageViewSet(viewsets.ModelViewSet):
    queryset = ChatSessionMessage.objects.none()
    serializer_class = ChatSessionMessageSerializer
    permission_class = (permissions.IsAuthenticated(), )
    

    def get_queryset(self):
        queryset = ChatSessionMessage.objects.filter(Q(chat_session__created_by=self.request.user) | Q(chat_session__received_by=self.request.user))
        session_id = self.request.query_params.get('sessionId', None)
        if session_id is not None:
            queryset = queryset.filter(chat_session__id=session_id)
        return queryset

    def create(self, request):
        data = json.loads(request.body)

        session = ChatSession.objects.get(id=data.get('sessionId'))
        
        message = ChatSessionMessage.objects.create(user=request.user, message=data.get('message'), chat_session=session)
        return response.Response(status=status.HTTP_201_CREATED)
