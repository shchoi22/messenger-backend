from rest_framework import serializers
from .models import ChatSession, ChatSessionMessage

from django.contrib.auth import get_user_model
User = get_user_model()

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'username']

class ChatSessionMessageSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = ChatSessionMessage
        fields = ['id', 'user', 'message', 'created', 'modified', 'read']

class ChatSessionSerializer(serializers.HyperlinkedModelSerializer):
    created_by = UserSerializer(read_only=True)
    received_by = UserSerializer(read_only=True)
    messages = ChatSessionMessageSerializer(read_only=True, many=True)
    other_party_username = serializers.SerializerMethodField()
    has_unread_message = serializers.SerializerMethodField()

    class Meta:
        model = ChatSession
        fields = ['id', 'created_by', 'received_by', 'messages', 'created', 'modified', 'other_party_username', 'has_unread_message']
    
    def get_other_party_username(self, obj):
        return obj.received_by.username if self.context['request'].user.username == obj.created_by.username else obj.created_by.username
    
    def get_has_unread_message(self, obj):
        return obj.messages.filter(read=False).exclude(user=self.context['request'].user).exists()


