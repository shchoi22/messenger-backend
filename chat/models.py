# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import uuid

from django.db import models
from django.contrib.auth import get_user_model
from django_extensions.db.models import TimeStampedModel

User = get_user_model()

class ChatSession(TimeStampedModel):   
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False) 
    created_by = models.ForeignKey(User, related_name='created_by', on_delete=models.PROTECT)
    received_by = models.ForeignKey(User, related_name='received_by', on_delete=models.PROTECT)



class ChatSessionMessage(TimeStampedModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    chat_session = models.ForeignKey(ChatSession, related_name='messages', on_delete=models.PROTECT)
    message = models.TextField(max_length=1000)
    read = models.BooleanField(default=False)

    class Meta:
        ordering = ['created']
    
    @property
    def to_user(self):
        if self.chat_session.received_by == self.user:
            return self.chat_session.created_by
        else:
            return self.chat_session.received_by
    

