from __future__ import absolute_import, unicode_literals


import os
import celery
 
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'messenger.settings')
 
app = celery.Celery('messenger')
app.config_from_object('django.conf:settings')
 
app.autodiscover_tasks()